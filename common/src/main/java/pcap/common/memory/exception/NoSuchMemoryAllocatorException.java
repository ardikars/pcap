package pcap.common.memory.exception;

public class NoSuchMemoryAllocatorException extends Exception {

  public NoSuchMemoryAllocatorException(String message) {
    super(message);
  }
}
