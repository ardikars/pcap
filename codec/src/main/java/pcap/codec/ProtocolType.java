package pcap.codec;

public interface ProtocolType<T extends Number> {

  T value();
}
