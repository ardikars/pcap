/** This code is licenced under the GPL version 2. */
package pcap.spi;

/**
 * As returned by the pcap_stats().
 *
 * @author <a href="mailto:contact@ardikars.com">Ardika Rommy Sanjaya</a>
 * @since 1.0.0
 */
public interface Statistics extends Status {}
